# Dell_G3_3579_OpenCore_Bootloader

#### 语言 Language

<a href="https://gitee.com/kp296/dell_g3_3579_opencore_bootloader/blob/master/README.md">中文</a> English

#### Description
It is suitable for Dell G3 3579 i7-8750h processor and Mac OS starter of notebook computer without thunderbolt 3 interface.

#### Update instructions

> V6.0 Update macOS to 12.2.1 , I'm waiting for someone to repair the blackscreen problem, because I have no time.

#### Hardware configuration

>CPU:Intel Core i7-8750H
>
>IGPU:Intel UHD 630
>
>DGPU:NVIDIA GeForce GTX 1050Ti(Not Working)
>
>Hard Disk:<s>LITEON 128GiB SSD</s>Replace with Western Digital SN550 NVMe SSD 1TB+Seagate 1TB HDD
>
>Wireless Network Adapter:Broadcom DW1820A
>
>Cable Network Adapter:Realtek RTL8111

#### grubx64.efi usage
> setup_var 0x5BC 0x0 (Disable CFG lock)

#### GitHub Project Address
https://github.com/VersionZKP2356/Dell-G3-3579-OpenCore-Boot-File