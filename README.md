# Dell G3 3579 OpenCore 引导器

#### 语言 Language

中文  <a href="https://gitee.com/kp296/dell_g3_3579_opencore_bootloader/blob/master/README.en.md">English</a>

#### 介绍
适用于Dell G3 3579 i7-8750H处理器，没有雷电3接口的笔记本电脑的macOS启动器。

#### 联系方式
[TG频道](https://t.me/vz2356dailypanel)  [QQ频道](https://qun.qq.com/qqweb/qunpro/share?_wv=3&_wwv=128&appChannel=share&inviteCode=1W4RwOd&appChannel=share&businessType=9&from=246610&biz=ka)

#### 更新说明
> V7.0 升级macOS 到 12.4版本，黑屏问题解决

#### 硬件配置
> CPU:Intel Core i7 8750H
>
> IGPU:Intel UHD 630
>
> DGPU:NVIDIA Geforce GTX 1050Ti(不工作)
>
> 硬盘:<s>LITEON 128GB SSD</s>用西数SN550 1TB NVMe SSD代替+希捷1TB HDD
>
> 无线网卡:Broadcom DW1820A
>
> 有线网卡:Realtek RTL8111

#### grubx64.efi用法
> setup_var 0x5BC 0x0 (关闭CFG锁）

#### GitHub项目地址
https://github.com/VersionZKP2356/Dell-G3-3579-OpenCore-Boot-File